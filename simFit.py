####################################################################################################
# This script has a couple functions for performing a simultaneous extended pdf fit on several
# categories. The key functions are:
# 
# Key setup functions:
# * histListToRdh | take list of TH1F histograms and a filter function, return RDH in workspace
# * setupPdfs     | take names (channel, sig, bkg) and build sig/bkg models for this channel
# * throwToy      | generate toys based on sig/bkg models from setupPdfs
# * 
# Key fit functions:
# * fixVariablesInWs / unFixVariablesInWs | freeze certain parameters in workspace
# * makeCombinedDataset   | make a combined dataset, combined sig/bkg model
# * doNll                 | perform NLL scan and fit
#
# In the main function at the bottom, is an example of how I normally use these functions. There
# are two loops: one to set up the models channel by channel
# another to perform the simultaneous fits
#
# Aaron White aaronsw@umich.edu
####################################################################################################




### imports
from __future__ import division
import sys
sys.path.append("/home/prime/hmumu/ana-200218/code/")
sys.path = ["/home/prime/Downloads/buildRoot/lib/"] + sys.path
import histograms
import cPickle as pickle
import glob,os,time, random, math, time
import numpy as np

sys.argv.append( '-b' )
from ROOT import TH1F, RooWorkspace, RooDataHist, RooArgList, TCanvas, RooFit, TText, TPad
from ROOT import TLine, gPad, RooArgSet, RooAddPdf,RooCategory,RooDataSet,RooCmdArg,RooSimultaneous
from ROOT import RooLinkedList, RooAbsReal, TFile
from ROOT.RooFit import *
from ROOT import kGreen, kRed, kFALSE, kBlue

# poisson limit function
# import poissonLimitFunction

# fix random seed for toys
import random
random.seed(125)
from ROOT import RooRandom
RooRandom.randomGenerator().SetSeed(123)
# RooRandom.randomGenerator().SetSeed(random.randint(0,1e10))


### imports for creating std::map
import ROOT
ROOT.gInterpreter.GenerateDictionary("std::pair<std::const string, RooDataSet*>", "map;string;RooDataSet.h")
### imports for creating std::map

def wAdd(w,s):
  # wrapper for adding s to workspace w
  # it's nice to have the printout for debugging
  print "**************************"
  print "===="
  print repr(s)
  print "===="
  w.factory(s)
  print "**************************"

class RootIterator(object):
    """A wrapper around the ROOT iterator so that it can be used in python"""
    def __init__(self, o):
        if hasattr(o,'Class') and o.Class().InheritsFrom('TIterator'):
            self.iter = o
        elif hasattr(o,'createIterator'):
            self.iter = o.createIterator()
        elif hasattr(o,'MakeIterator'):
            self.iter = o.MakeIterator()
        elif hasattr(o,'componentIterator'):
            self.iter = o.componentIterator()
        else:
            self.iter = None
    def __iter__(self):
        return self
    def next(self):
        n = self.iter.Next()
        if n is None or not n:
            raise StopIteration()
        return n

# quick function for giving root objects random names
def n(): return str(random.randint(0,1e20))

def loadHistogramsFromPickle(path):
  # load histograms from path
  # [hist1, hist2,...,histN]
  print "Loading hist", path
  f=open(path,"r")
  hists=pickle.load(f)
  print "Done loading hist", path
  return hists

def histToTh1(hist):
  # convert hist to th1f
  print "### histToTh1 ###"
  bins=hist.getBinning()
  ret = TH1F(hist.name,hist.name,bins.nBins-0,bins.binMin/1e3,bins.binMax/1e3)
  weightsSquared = hist.weightsSquared
  for i, data in enumerate(hist.getData()):
    # pos=(i/bins.nBins)*(bins.binMax-bins.binMin) + bins.binMin
    # ret.Fill(pos,data)
    ret.SetBinContent(i+1,data)
    ret.SetBinError(i+1,math.sqrt(weightsSquared[i]))
    # if data>0:
      # ret.SetBinError(i,math.sqrt(data))
  return ret

def txt(x, y, t):
  # wrapper for terrible TText
  p2 = TText(x,y,str(t));
  p2.SetTextSize(0.1);
  return p2

def sigPlot(w, rdhName, fitPdfName, name,pullOn=True, showSig=False):
  # make signal included plot
  print "\n\n"
  print "#"*50
  print "$$$$ quick.Plot $$$$ "
  print "rdhName",rdhName
  print "fitPdfName",fitPdfName
  print "name",name
  print "showSig",showSig
  rdh=w.obj(rdhName)
  # rdh=w.obj("combDataRds") # use to check that combined dataset is made properly
  fitPdf=w.pdf(fitPdfName)
  print "RDH:",; rdh.Print()
  print "fitPdf:",; fitPdf.Print()
  print "N rdh:", rdh.sumEntries()
  print "#"*50
  # pull params
  sigPdf=w.pdf(showSig["sigPdfName"])
  bkgPdf=w.pdf(showSig["bkgPdfName"])
  nSig=w.obj(showSig["sigCoefName"]).getVal()
  nSigPred=w.var(showSig["nSigName"]).getVal()
  nBkg=w.var(showSig["nBkgName"]).getVal()

  c = TCanvas(str(time.time()),str(time.time()),700,700);
  p1 = TPad("top","top",0,0.1,1,1); p1.Draw(); p1.cd()
  gPad.SetBottomMargin(0);
  x = w.var("x");
  frame = x.frame(RooFit.Title("Category: {0}".format(name)));
  rdh.plotOn(frame,RooFit.Name("hist"),RooFit.LineColor(1),RooFit.LineWidth(1),RooFit.MarkerSize(1),RooFit.MarkerColor(1));
  fitPdf.plotOn(frame,RooFit.Normalization(nBkg+nSig,RooAbsReal.NumEvent),RooFit.Name("fitPdf"),RooFit.LineColor(kRed),RooFit.LineWidth(2));
  bkgPdf.plotOn(frame,RooFit.Normalization(nBkg,RooAbsReal.NumEvent),RooFit.Name("bkgPdf"),RooFit.LineColor(kBlue),RooFit.LineWidth(4));
  sigPdf.plotOn(frame,RooFit.Normalization(nSig,RooAbsReal.NumEvent),RooFit.Range("fullMassRange"),RooFit.Name("signal"),RooFit.LineColor(kGreen),RooFit.LineWidth(2));

  frame.Draw();
  c.Update();
  c.SaveAs("plots/rooFitCategory-{0}.png".format(name));
  print "####### quick.Plot Done"

def zeroInRange(w,rdh,low,high):
  # make th1f from rdh, zero values between low and high, return as rdh
  # used for blinding when making a plot
  # make th1f
  hist = rdh.createHistogram(n(),w.var("x"))
  # set variables to zero
  for i in range(hist.GetNbinsX()+1):
    x = hist.GetBinCenter(i)
    v = hist.GetBinContent(i)
    if x>low and x<high:
      hist.SetBinContent(i,0)
  # convert back to rdh
  rdh = makeRdsFromTh1f(hist,rdh.GetName(),w.var("x"))
  return rdh

def quickPlot(w, rdhName, bkgPdfName, name,pullOn=True, showSig=False,blind=False):
  print "#"*50
  print "$$$$ quick.Plot $$$$ "
  print "rdhName",rdhName
  print "bkgPdfName",bkgPdfName
  print "name",name
  print "showSig",showSig
  rdh=w.obj(rdhName)
  fitPdf=w.pdf(bkgPdfName)
  print "RDH:",; rdh.Print()
  print "fitPdf:",; fitPdf.Print()
  print "N rdh:", rdh.sumEntries()
  print "#"*50
  c = TCanvas(str(time.time()),str(time.time()),700,700);
  p1 = TPad("top","top",0,0.5,1,1); p1.Draw(); p1.cd()
  gPad.SetBottomMargin(0);
  x = w.var("x");
  frame = x.frame(RooFit.Title("Category: {0}".format(name)));
  frame.SetYTitle("Events")
  frame.SetXTitle("Muu")
  frame.SetLabelSize(0.0)
  frame.SetTitleSize(0.1,"Y")
  frame.SetTitleSize(0,"X")
  frame.SetTitleOffset(0.4,"Y")
  # rdh.plotOn(frame,RooFit.Name(rdh.GetName()),RooFit.Range("bkgFitRangeL,bkgFitRangeH"),RooFit.LineColor(1),RooFit.LineWidth(1),RooFit.MarkerSize(1),RooFit.MarkerColor(1));
  if blind: rdh = zeroInRange(w,rdh,w.var("srLow").getVal(),w.var("srHigh").getVal())
  rdh.plotOn(frame,RooFit.Name(rdh.GetName()),RooFit.LineColor(1),RooFit.LineWidth(1),RooFit.MarkerSize(1),RooFit.MarkerColor(1));

  # w.var("x").setRange("bkgFitRangeL",110,w.var("srLow").getVal())
  # w.var("x").setRange("bkgFitRangeH",w.var("srHigh").getVal(),160)

  if blind: fitPdf.plotOn(frame,RooFit.Normalization(1,RooAbsReal.Relative),RooFit.Range("bkgFitRangeL,bkgFitRangeH"),RooFit.Name("fit"),RooFit.LineColor(kRed),RooFit.LineWidth(4));
  else:     fitPdf.plotOn(frame,RooFit.Normalization(1,RooAbsReal.Relative),RooFit.Name("fit"),RooFit.LineColor(kRed),RooFit.LineWidth(4));
  # fitPdf.plotOn(frame,RooFit.Name("fit"),RooFit.LineColor(kRed),RooFit.LineWidth(4),RooFit.MarkerColor(kRed));
  # w.pdf("bkgFit").paramOn(frame)
  nParams=int(w.var("nParams"+firstUpper(bkgPdfName)).getVal())
  chi2 = frame.chiSquare("fit",rdh.GetName(),nParams)
  print "#"*50
  print "Chi2", name, chi2
  pY = 1.5*(frame.GetMinimum()+frame.GetMaximum())/2
  pX = (160+110)/2
  print pX, pY
  print "#"*50
  frame.addObject(txt(1.0*pX,pY*0.90,"Chi2={0:.3f}".format(chi2)))
  hpull=frame.pullHist()
  hresid=frame.residHist()
  if showSig:
    print "\n#############################"
    print "### Adding signal to plot ###"
    print "#############################"
    sigPdf=w.pdf(showSig["sigPdfName"])
    bkgPdf=w.pdf(showSig["bkgPdfName"])
    nSig=w.obj(showSig["sigCoefName"]).getVal()
    nSigPred=w.var(showSig["nSigName"]).getVal()
    nBkg=w.var(showSig["nBkgName"]).getVal()
    bkgPdf.plotOn(frame,RooFit.Normalization(1,RooAbsReal.Relative),RooFit.Name("bkgPdf"),RooFit.LineColor(kBlue),RooFit.LineWidth(2),RooFit.MarkerColor(kBlue));
    sigPdf.plotOn(frame,RooFit.Normalization(nSig*100,RooAbsReal.NumEvent),RooFit.Range("fullMassRange"),RooFit.Name("signal"),RooFit.LineColor(kGreen),RooFit.LineWidth(2),RooFit.MarkerColor(kGreen));
    frame.addObject(txt(130e3,pY*1.1,"nSig(pdctd)={0:.2f}({1:.2f}) nBkg={2}".format(nSig,nSigPred,int(nBkg))))
    frame.addObject(txt(130e3,pY*0.8,"chi2={0:.2f}".format(chi2)))
  #
  frame.Draw();
  #
  if pullOn:
    # make pull plot
    c.cd()
    p3 = TPad("mid","mid",0,0.25,1,0.5); p3.Draw(); p3.cd()
    gPad.SetBottomMargin(0);
    gPad.SetTopMargin(0);
    frame3 = x.frame(RooFit.Title(" "));
    frame3.SetYTitle("Residual")
    frame3.SetLabelSize(0.0)
    frame3.SetLabelSize(0.1,"Y")
    frame3.SetTitleSize(0.2,"Y")
    frame3.SetTitleOffset(0.2,"Y")
    frame3.addPlotable(hresid,"P") ;
    line12 = TLine(w.var("x").getMin(),0,w.var("x").getMax(),0); frame3.addObject(line12);
    frame3.Draw();
    #
    # make pull plot
    c.cd()
    p2 = TPad("mid","mid",0,0.0,1,0.25); p2.Draw(); p2.cd()
    gPad.SetTopMargin(0);
    frame2 = x.frame(RooFit.Title(" "));
    frame2.SetYTitle("Pull")
    frame2.SetXTitle("Muu")
    frame2.addPlotable(hpull,"P") ;
    frame2.SetLabelSize(0.1,"Y")
    frame2.SetLabelSize(0.12,"X")
    frame2.SetTitleSize(0.2,"Y")
    frame2.SetTitleSize(0.15,"X")
    frame2.SetTitleOffset(0.2,"Y")
    frame2.SetTitleOffset(0.5,"X")
    line11 = TLine(w.var("x").getMin(),0,w.var("x").getMax(),0); frame2.addObject(line11);
    frame2.Draw();
    gPad.SetBottomMargin(0.25);
  #
  c.cd()
  c.Update();
  c.SaveAs("plots/rooFitCategory-{0}.png".format(name));
  print "####### quick Plot Done"


def firstUpper(s):
  return s[0].upper()+s[1:]

def setupPdfs(w,name,sigName,bkgName,nBkgName,nSigName,sigCoefName,extended=True,plotOn=0):
  # make individual sig and bkg pdfs
  # do shape fits to signal and background
  # at the end of this, the PDF's for this channel should be ready for simultaneous fitting
  # returns names of fits
  print "======= setupPdfs called ==="
  print "name =",name
  print "sigName =",sigName
  print "bkgName =",bkgName
  print "nBkgName =",nBkgName
  print "nSigName =",nSigName
  print "sigCoefName =",sigCoefName
  print "============================"

  bkgPdfName="bkgPdf{0}".format(firstUpper(name))
  sigPdfName="sigPdf{0}".format(firstUpper(name))
  addPdfName="addPdf{0}".format(firstUpper(name))

  ########################################################
  # create pdfs
  # these options can be used to make your own PDF
  ########################################################
  ## diphot5
  # bkgPdfString = "EXPR::{0}('((1 - (((x/160)/13.)^(0.5)))^(1.*bParam0{1}))*((x/160)^(bParam1{1} + bParam2{1}*log((x/160)/13.) + bParam3{1}*(log((x/160)/13.)^2) + bParam4{1}*(log((x/160)/13.)^3)))*0.002495/(pow(0.091187-(x/1),2)-pow(0.002495,2))',x,bParam0{1}[5,5,6],bParam1{1}[-4.5,-10,1],bParam2{1}[-3.0442,-10, 10],bParam3{1}[-9.29259e-01,-1.,1.],bParam4{1}[-8.51735e-02,-10,10])".format(bkgPdfName,firstUpper(name))
  ## expow
  # bkgPdfString = "EXPR::{0}('(x/(160)^bParam1{1})*((2.718)^((-bParam2{1}*x/160)))',x,bParam1{1}[-4.,-100.,100.],bParam2{1}[3,-100.,100.])".format(bkgPdfName,firstUpper(name))
  ## spf
  ## bkgPdfString = "EXPR::{0}('((x/160))*(((1-log(2.718*((x/160)/13)^bParam1{1}))^bParam0{1}))*((x/160)^(bParam2{1} + bParam3{1}*log((x/160)/13.) + bParam4{1}*(log((x/160)/13.)^2) + bParam5{1}*(log((x/160)/13.)^3)))',x,bParam0{1}[1.41224e+01,0.001,20],bParam1{1}[4.64978,0,5],bParam2{1}[-2.9276e+00,-10.,0.],bParam3{1}[7.0359e-01,-5, 5.],bParam4{1}[3.0952e-01,-1.,1.],bParam5{1}[3.8697e-02,-1.,1.])".format(bkgPdfName,firstUpper(name))
  ## print bkgPdfString; quit()
  ##
  # sigPdfString="RooGaussian::{0}(x,sMean{1}[125,120,130],sSigma{1}[2,1,5])".format(sigPdfName,firstUpper(name))
  # w.factory(sigPdfString); w.factory("nParams{0}[3]".format(firstUpper(sigPdfName)))
  # w.factory(bkgPdfString); w.factory("nParams{0}[5]".format(firstUpper(bkgPdfName)))
  ########################################################


  #########################################################
  ## Use Hmumu pdf
  #########################################################
  # hmumu fitting function bkg, direct from the idiot's code
  wAdd(w,"RooVoigtian::BW_bg{0} (x, mBW{0}[91.2], ZWidth{0}[2.49], sigma{0}[2])".format(firstUpper(name))) 
  wAdd(w,"EXPR::exp_bg{0}('exp(a2_bg{0}*(x/1))*(1./pow((x/1), 3))', x,  a2_bg{0}[-0.1,-1,1])".format(firstUpper(name)))
  wAdd(w,"SUM::{0}( BW_bg{1} , frac_bg{1}[0.2, 0.0, 1] * exp_bg{1} )".format(bkgPdfName,firstUpper(name))) 
  wAdd(w,"nParams{0}[2]".format(firstUpper(bkgPdfName)))
  # hmumu fitting function sig, direct from the idiot's code
  wAdd(w,"BW_mean_sig{0}[125,121,129]".format(firstUpper(name)))
  wAdd(w,"BW_mean_sig_gaus{0}[125,123,127]".format(firstUpper(name))) 
  wAdd(w,"RooCBShape::sigCB{0}(x, BW_mean_sig{0}, CB_sigma_sig{0}[2,1,4], CB_a_sig{0}[1.75,1,2], CB_n_sig{0}[1.])".format(firstUpper(name)))
  wAdd(w,"RooGaussian::sigGF{0}(x, BW_mean_sig_gaus{0}, BW_sigma_sig{0}[5,1,6])".format(firstUpper(name)))
  wAdd(w,"SUM:{0}(sigCB{1}, frac_sig{1}[0.3,0, 0.4]* sigGF{1})".format(sigPdfName,firstUpper(name)))
  wAdd(w,"nParams{0}[4]".format(firstUpper(sigPdfName)))
  ########################################################

  if extended: # do extended pdf
    # build extended pdf following https://www.nikhef.nl/~vcroft/RooFit.html
    # nSigCoef is a product mu*nsig
    addPdfString="SUM::{0}({1}*{2},{3}*{4})".format(addPdfName,nBkgName,bkgPdfName,sigCoefName,sigPdfName)
    wAdd(w,addPdfString)
  else:
    addPdf = RooAddPdf(addPdfName,addPdfName,w.pdf(sigPdfName),w.pdf(bkgPdfName),w.var("mu"))
    getattr(w,"import")(addPdf)
  wAdd(w,"nParams{0}[3]".format(firstUpper(addPdfName)))

  # fit(w,bkgPdfName,bkgName,fitRange="bkgFitRangeL,bkgFitRangeH")
  fit(w,bkgPdfName,bkgName,fitRange="fullMassRange")
  fit(w,sigPdfName,sigName,fitRange="fullMassRange")
  # diagnostic plots
  if plotOn: quickPlot(w, bkgName, bkgPdfName,"singleFitBkg-"+name,pullOn=True,blind=True)
  if plotOn: quickPlot(w, sigName, sigPdfName,"singleFitSig-"+name,pullOn=True)
  return bkgPdfName,sigPdfName,addPdfName

def fit(w,pdfName,rdhName,fitRange=None):
  # fit wrapper
  print "#"*10
  print "Fitting"
  print pdfName, rdhName
  print "#"*10
  if fitRange:
    result=w.pdf(pdfName).fitTo(w.obj(rdhName),RooFit.Range(fitRange));
  else:
    result=w.pdf(pdfName).fitTo(w.obj(rdhName),RooFit.Range(110,160));
  # w.pdf(pdfName).chi2FitTo(w.obj(rdhName),RooFit.Range(110,160));
  # result=w.pdf(pdfName).fitTo(w.obj(rdhName),RooFit.Range(110,160));

def makeRdsFromTh1f(hist,name,x):
  # take th1f hist, return approximate unbinned dataset
  # unbinned dataset uses bin centers
  rds = RooDataSet(name,name,RooArgSet(x))
  # loop over bins in th1f hist
  for i in range(1,hist.GetNbinsX()+1):
    histValue =hist.GetBinContent(i)
    histCenter=hist.GetBinCenter(i)
    x.setVal(histCenter)
    for j in range(int(histValue)): rds.add(RooArgSet(x))
  #####################
  ## Diagnostic plot ##
  # c = TCanvas(str(time.time()),str(time.time()),700,700);
  # frame = x.frame(RooFit.Range("fullMassRange"),RooFit.Title("Title")) ;
  # rds.plotOn(frame,RooFit.Name("hist"),RooFit.LineColor(1),RooFit.LineWidth(1),RooFit.MarkerSize(1),RooFit.MarkerColor(1));
  # frame.Draw()
  # c.Update();
  # c.SaveAs("plots/debug.png");
  # quit()
  #####################
  return rds

def histListToRdh(w,hlist, name, cut):
  # convert list of hists to th1f, then to rdh, and also rds 
  # NOTE: (rds is generated from the hist!)
  # returns number of signal events in signal region (nInSr) and total events (nTot)
  global numBins
  print "### histListToRdh ###"
  hist=None
  for h in hlist:
    if cut(h):
      if hist==None: 
        hist=histToTh1(h)
        hist.SetName(name); hist.SetTitle(name)
      else: hist.Add(histToTh1(h))
  # convert to rdh
  rdh = RooDataHist(name,name, RooArgList(w.var("x")), hist);
  rds = makeRdsFromTh1f(hist,name+"Rds",w.var("x"))
  print rdh
  getattr(w,"import")(rdh)
  getattr(w,"import")(rds)
  # add number of entries
  wAdd(w,"nEvents{0}[{1}]".format(firstUpper(name),rdh.sumEntries()))
  if numBins==None: numBins=rdh.numEntries()
  elif numBins!=rdh.numEntries(): raise BaseException("Aaron you messed up the bins")
  nInSr = rdh.sumEntries("x>{0} && x<{1}".format(w.var("srLow").getVal(),w.var("srHigh").getVal()))
  nTot  = rdh.sumEntries()
  return nInSr, nTot

def throwToy(w,bkgPdfName,sigPdfName,rdhName,nInjectSignal=None,nEvents=1000,nToys=1):
  # throw toys from PDF called bkgFit
  print "### Throw Toy ###"
  print "bkgPdfName",bkgPdfName
  print "rdhName",rdhName
  print "nEvents",nEvents
  print "nToys",nToys
  print "nInjectSignal",nInjectSignal
  bkgPdf=w.pdf(bkgPdfName)
  x = w.var("x")
  toyRdhs = []
  toyRdss = []
  for i in range(nToys):
    # make background toys
    toyRdh = bkgPdf.generateBinned(RooArgSet(x),RooFit.NumEvents(nEvents))
    toyRdh.SetName("{0}ToyRdh{1}".format(rdhName,i))
    toyRdh.SetTitle("{0}ToyRdh{1}".format(rdhName,i))
    toyRds = bkgPdf.generate(RooArgSet(x),int(nEvents))
    # toyRds = bkgPdf.generate(RooArgSet(x),RooFit.NumEvents(nEvents))
    toyRds.SetName("{0}ToyRds{1}".format(rdhName,i))
    toyRds.SetTitle("{0}ToyRds{1}".format(rdhName,i))
    # make signal injection toys
    if nInjectSignal!=None:
      remainder=nInjectSignal%1 # inject<1 this pulls up injection
      nInjectSignal=int(nInjectSignal)
      # with probability of remainder, increment by 1
      nInjectSignal+=[0,1][random.random()<=remainder]
      # in not injecting, continue
      if nInjectSignal>0: 
        print "### DEBUG: injecting signal n=",nInjectSignal
        sigPdf=w.pdf(sigPdfName)
        sigToyRds = sigPdf.generate(RooArgSet(x),nInjectSignal)
        sigToyRdh = sigPdf.generateBinned(RooArgSet(x),RooFit.NumEvents(nInjectSignal))
        # append these to bkg sets
        toyRdh.add(sigToyRdh)
        toyRds.append(sigToyRds)
    getattr(w,"import")(toyRdh)
    getattr(w,"import")(toyRds)
    toyRdhs.append(toyRdh)
    toyRdss.append(toyRds)
  print "### DEBUG: throwToy: rdh entries=", toyRdhs[0].sumEntries()
  print "### DEBUG: throwToy: rds entries=", toyRdss[0].sumEntries()
  print "### DEBUG: throwToy: rds printout:", toyRdss[0].Print()
  return toyRdhs, toyRdss

def fixVariablesInWs(w,skipFix=lambda x: True):
  # fix all variables in workspace
  allVars=w.allVars()
  iterate=allVars.createIterator()
  var=iterate.Next()
  origRanges={} # dictionary to keep track of variable ranges
  while var!=None:
    if not skipFix(var.GetName()):
      origRanges[var.GetName()] = {"val":var.getVal(),"min":var.getMin(),"max":var.getMax()}
      curVal=var.getVal()
      var.setMax(curVal)
      var.setMin(curVal)
      print "fixing variable", var.GetName(), "to", curVal
      var.Print()
    var=iterate.Next()
  return origRanges

def unFixVariablesInWs(w,origRanges):
  # un-fix values by restoring ranges to those from origRanges
  for varName in origRanges.keys():
    orig=origRanges[varName]
    w.var(varName).setVal(orig["val"])
    w.var(varName).setMin(orig["min"])
    w.var(varName).setMax(orig["max"])

def makeCombinedDataset(w,channels,toyId=0,plotOn=0):
  # make combined dataset
  # 
  # define categories
  print "#"*50
  print "Making combined dataset"
  print "#"*50
  categories = RooCategory("channelName","channelName") ;
  for i,chan in enumerate(channels):
    print "### DEBUG: making channel defineType", chan["name"]
    name=chan["name"]
    categories.defineType(name)
  getattr(w,"import")(categories)

  # make combined dataset
  dataSetsKeep=[]
  dataMap = ROOT.std.map('string, RooDataSet*')()
  dataMap.keepalive = list()
  for i,chan in enumerate(channels):
    print "### DEBUG: combining dataset", chan["name"]
    name=chan["name"]
    rdsName=chan["toFitRdsName"]
    rds=w.obj(rdsName)
    rds.Print()
    dataSetsKeep.append(rds) # force rds stay in memory
    dataMap.keepalive.append(rds)
    if rds.numEntries()==0: print "failed"; quit()

    # follow instructions: https://root-forum.cern.ch/t/combining-roodatasets-using-std-map-in-pyroot/16471/19
    pair=ROOT.std.pair("const string, RooDataSet*")(name, rds)
    dataMap.insert(dataMap.cbegin(), pair)
  combDataRds = RooDataSet("combDataRds","combDataRds",RooArgSet(w.var("x")),RooFit.Index(categories),RooFit.Import(dataMap))
  combDataRdh = combDataRds.binnedClone("combDataRdh","combDataRdh")
  getattr(w,"import")(combDataRds)
  getattr(w,"import")(combDataRdh)

  # make simultaneous pdf
  simPdf = RooSimultaneous("simPdf","simPdf",categories)
  for i,chan in enumerate(channels):
    print "### DEBUG: make simultaneous pdf", chan["name"]
    name=chan["name"]
    addPdfName=chan["addPdfName"]
    print "### DEBUG: addPdfName",addPdfName
    simPdf.addPdf(w.pdf(chan["addPdfName"]),name)
  getattr(w,"import")(simPdf)

  if plotOn: 
    for i,chan in enumerate(channels):
      print "### DEBUG: making plot for", chan["name"]
      name=chan["name"]
      dataName=chan["toFitRdhName"]
      pdfName=chan["addPdfName"]
      sigName=chan["sigPdfName"]
      bkgName=chan["bkgPdfName"]
      nSig=w.var(chan["nSigName"]).getVal() * w.var("mu").getVal()
      nSigPred=w.var(chan["nSigName"]).getVal() 
      nBkg=w.var(chan["nBkgName"]).getVal()
      sigDict={"sigPdfName":sigName,"nSig":nSig,"nBkg":nBkg,"bkgName":bkgName,"nSigPred":nSigPred}
      quickPlot(w,dataName,pdfName,"combinedFit{0}-{1}".format(toyId,name), showSig=chan)

####################################################################################
### Intersect finder
####################################################################################
def findIntersect_helper(x,y,intersect,bestPoint,bestI,difToTarget,checkPointsX):
  # print "\nStart new minimization with", checkPointsX
  for i,point in enumerate(checkPointsX):
    x.setVal(point)
    thisDif=abs(intersect - y.getVal())
    # print "thisDif",thisDif,difToTarget
    if thisDif<difToTarget:
      print "debug difto", thisDif,difToTarget
      difToTarget=thisDif
      bestPoint=point
      bestI=i
      # print "\t New best point", bestPoint, thisDif, i
  return bestPoint,bestI,difToTarget 

def findIntersect(x,y,intersect,cut=lambda x:True):
  # find what y(x) = intersect
  # cut is used to force x into a range (for example lambda x: x>10)
  #   this should be used in case there are multiple minima (ie nll)
  print "#"*22
  print "### Find intersect ###"
  print "#"*22
  limMin=x.getMin()
  limMax=x.getMax()
  res=5000
  difToTarget=1e99
  bestPoint=limMin
  bestI=0
  checkPointsX=np.linspace(limMin,limMax,res)
  checkPointsX=filter(cut,checkPointsX)
  for step in range(10):
    bestPointOld = bestPoint
    bestPoint,bestI,difToTarget = findIntersect_helper(x,y,intersect,bestPoint,bestI,difToTarget,checkPointsX)
    if bestPoint==bestPointOld: ## did not find better point
      break
    limMin=checkPointsX[max(0,bestI-1)]
    limMax=checkPointsX[min(bestI+1,len(checkPointsX)-1)]
    checkPointsX=np.linspace(limMin,limMax,res)
    checkPointsX=filter(cut,checkPointsX)
  print "### Done ###"
  print "bestPoint", bestPoint
  print "############"
  # print "#"*12
  return bestPoint
####################################################################################

def exportNll(nll,mu,lowLim,highLim,nllMinFit,toyId=0):
  # export nll from lowLim to highLim
  res=100
  points=np.linspace(lowLim,highLim,res)
  nllVals=[]
  muVals=[]
  for point in points:
    mu.setVal(point)
    nllVals.append(nll.getVal())
    muVals.append(point)
  export={"name":"nll{0}".format(toyId),"dataY":nllVals,"dataX":muVals}
  f=open("plots/nllDump.py","a")
  f.write(str(export)+"\n")
  f.close()
  #
  f=open("plots/nllAtMu0.txt","a")
  mu.setVal(0)
  f.write(str(nll.getVal()-nllMinFit)+"\n")
  f.close()

def doNll(w,toyId=0,channels=[]):
  # do nll fit
  print "##### Doing NLL #####"
  nll = w.pdf("simPdf").createNLL(w.obj("combDataRdh"),RooFit.NumCPU(4))
  nll.SetTitle("nll"); nll.SetName("nll")
  getattr(w,"import")(nll)
  print "##################"
  # a=ROOT.RooMinuit(nll)
  print "### DEBUG: hereStartMinuit"
  ROOT.RooMinuit(nll).migrad()
  print "### DEBUG: hereEndMinuit"
  muMinFit=w.var("mu").getVal()
  nllMinFit=nll.getVal()

  # find automatic limits for plot
  nnlLim=2
  # nnlLim=0.05
  lowLim=findIntersect(w.var("mu"),nll,nnlLim+nllMinFit,cut=lambda x:x<muMinFit)
  highLim=findIntersect(w.var("mu"),nll,nnlLim+nllMinFit,cut=lambda x:x>muMinFit)
  lowLim=-3; highLim=1;
  # export nll data to text file
  exportNll(nll,w.var("mu"),lowLim,highLim,nllMinFit,toyId=toyId)
  # save nll to workspace


  # make plot
  c = TCanvas(str(time.time()),str(time.time()),700,700);
  c.cd()
  # frame = w.var("mu").frame(RooFit.Title("LL and profileLL in mean")) ;
  frame = w.var("mu").frame(RooFit.Range(lowLim,highLim),RooFit.Title("LL and profileLL in mean")) ;
  nll.plotOn(frame,RooFit.ShiftToZero())
  # frame.addObject(txt(muMinFit,0,"mu={0:.2f}".format(muMinFit)))
  frame.GetYaxis().SetRangeUser(0.,1.5*2);
  frame.Draw()
  gPad.SetBottomMargin(0.10);
  gPad.SetLeftMargin(0.15);
  c.Update();
  c.SaveAs("plots/nll{0}.png".format(toyId));

  w.var("mu").setVal(muMinFit)
  print "##################"
  print "NLL: ",
  nll.Print()
  print "mu:",
  w.var("mu").Print()
  print "##################"
  print "NLL Done"

  # make plot for NLL
  for i,chan in enumerate(channels):
    print "### DEBUG: making plot for", chan["name"]
    name=chan["name"]
    dataName=chan["toFitRdhName"]
    pdfName=chan["addPdfName"]
    sigName=chan["sigPdfName"]
    bkgName=chan["bkgPdfName"]
    nSig=w.var(chan["nSigName"]).getVal() * w.var("mu").getVal() 
    nSigPred=w.var(chan["nSigName"]).getVal() 
    nBkg=w.var(chan["nBkgName"]).getVal()
    sigDict={"sigPdfName":sigName,"nSig":nSig,"nBkg":nBkg,"bkgName":bkgName,"nSigPred":nSigPred}
    sigPlot(w,dataName,pdfName,"combinedFit{0}-{1}".format(toyId,name), showSig=chan)
    print "DEBUG: Done making plot for", chan["name"]
  print "DEBUG: Done making plots"


def doLimit(w, channels):
  ''' Calculate CLs limit'''
  # integrate nExpected from background in all channels
  # only use sigRegion
  import poissonLimitFunction
  bkgSum=0
  sigSum=0
  datSum=0
  obsSum=0
  x = w.var("x")
  for i,chan in enumerate(channels):
    print "$"*200, "\nConsidering", i
    # get dataset
    dataName=chan["toFitRdhName"]
    dataRdh=w.obj(dataName)
    nDat = dataRdh.sumEntries()
    nObs = dataRdh.sumEntries("x>{0} && x<{1}".format(w.var("srLow").getVal(),w.var("srHigh").getVal()))
    # get bkg fit
    bkgPdfName=chan["bkgPdfName"]
    bkgPdf = w.pdf(bkgPdfName)
    nBkg   = bkgPdf.createIntegral(RooArgSet(x),"fullMassRange").getVal()
    nBkgSR = bkgPdf.createIntegral(RooArgSet(x),"sigRegion").getVal()
    nBkgExpected = nBkgSR*(nDat/nBkg)
    # get signal fit
    sigPdfName=chan["sigPdfName"]
    sigPdf   = w.pdf(sigPdfName)
    nSigOrig = chan["origNsigValue"] # expected yield
    nSig     = sigPdf.createIntegral(RooArgSet(x),"fullMassRange").getVal()
    nSigSR   = sigPdf.createIntegral(RooArgSet(x),"sigRegion").getVal()
    nSigExpected = nSigSR*(nSigOrig/nSig)
    if (1):
      print "### Channel:\t",chan["name"]
      print "### nObs:\t",nObs
      print "### nDat:\t",nDat
      print "### nBkg:\t",nBkg
      print "### nBkgSR:\t",nBkgSR
      print "### Normalized background:\t", nBkgExpected
      print "### nSig:\t",nSig
      print "### origNsigValue:\t",chan["origNsigValue"]
      print "### nSigSR:\t",nSigSR
      print "### Normalized signal:\t", nSigExpected
      print "### === expectation ==="
      print "### nBkgSr - expected:", chan["nBkgSr"]
      print "### nSigSr - expected:", chan["nSigSr"]
      print "DEBUGY:", "name=",chan["name"], "nBkgSR=",nBkgSR, "nSigSR=",nSigSR, "nObs=",nObs, "origNsig=",chan["origNsigValue"]
    datSum+=nDat
    obsSum+=nObs
    bkgSum+=nBkgExpected
    sigSum+=nSigExpected

  limit={}
  limit=poissonLimitFunction.poissonLimit(bkgSum,sigSum,obsSum,nSpur=1)
  print "*"*100
  print "*"*100
  limit["nSig"] = sigSum;
  limit["nBkg"] = bkgSum;
  limit["nObs"] = datSum;
  for key in limit.keys(): print "###", key, "\t", limit[key]
  print "*"*100
  print "*"*100


def initWksp(injectScale):
  # script for setting up workspace settings
  from ROOT import RooAbsPdf
  integratorPrecision=1e-8
  RooAbsPdf.defaultIntegratorConfig().setEpsRel(integratorPrecision);
  RooAbsPdf.defaultIntegratorConfig().setEpsAbs(integratorPrecision);
  w = RooWorkspace("w","w")
  # create variable
  wAdd(w,"x[110,110,160]")
  # SR ranges defined based on variable so they can be used later
  wAdd(w,"srLow[120]")
  wAdd(w,"srHigh[130]")
  # various ranges defined
  # full mass range
  w.var("x").setRange("fullMassRange",110,160)
  # sidebands
  w.var("x").setRange("bkgFitRangeL",110,w.var("srLow").getVal())
  w.var("x").setRange("bkgFitRangeH",w.var("srHigh").getVal(),160)
  # signal region
  w.var("x").setRange("sigRegion",w.var("srLow").getVal(),w.var("srHigh").getVal())
  # signal strength mu is very sensitive to presets, so this takes a bit of fine tuning
  # wAdd(w,"mu[1,-10,10]")
  # wAdd(w,"mu[1,0,1e8]")
  if injectScale==0:
    # wAdd(w,"mu[1,-2,10]")
    # wAdd(w,"mu[1,-100000,10000]")
    wAdd(w,"mu[1,-100,100]")
  else:
    wAdd(w,"mu[{0},0,{1}]".format(abs(injectScale),10*abs(injectScale)))
  return w

if __name__ == "__main__":
  ####################################################################################################
  # Settings for running
  ####################################################################################################
  # set up workspace
  os.popen("rm plots/*")
  # coefficient scale to inject signal
  injectScale=0
  # injectScale=100
  # number of toys to throw
  nToys=1
  # quick = whether to run over all files
  quick=True
  # quick=False
  w = initWksp(injectScale)
  # loop through channels
  pickleDir = "/home/prime/hmumu/ana-200218/pickledHists-tmpMerged/*"
  # channels stores info about each channel
  channels=[]
  # global variable to sync binning
  numBins=None
  # list of categories to exclude from combined fit
  excludeChannels=["log","btag0","btag1","btag2","noCat0","noCat1","noCat2"]
  ####################################################################################################
  
  # loop over all category files, loading histograms and setting up models
  for i,path in enumerate(glob.glob(pickleDir)):
    name = path[path.rfind("-")+1:path.rfind(".")]
    if name=="log": continue
    name=firstUpper(name)
    sigName="sig"+name
    bkgName="bkg"+name
    # load histograms -- this is the function that should be changed to avoid using eventClass
    hists=loadHistogramsFromPickle(path)
    # import histograms to wksp
    nBkgSr,nBkg=histListToRdh(w, hists["muu"],bkgName,lambda x: x.isData)
    nSigSr,nSig=histListToRdh(w, hists["muu"],sigName,lambda x: x.name in ["zh","wh","tth","ggh","vbf"])
    print "DEBUGS:", name, nSig, nSigSr, nBkg,nBkgSr,nSigSr/nBkgSr
    # store numbers of events
    nSigName    ="nEvents{0}".format(firstUpper(sigName))
    nBkgName    ="nEvents{0}".format(firstUpper(bkgName))
    sigCoefName ="nSigCoef{0}".format(firstUpper(sigName))
    wAdd(w,"{0}[{1}]".format(nSigName,nSig))
    wAdd(w,"{0}[{1}]".format(nBkgName,nBkg))
    wAdd(w,"prod:{0}(mu,{1})".format(sigCoefName,nSigName))
    w.var("x").setBins(numBins)
    # create pdf's for fitting signal and background
    # setup and fit pdf's
    bkgPdfName,sigPdfName,addPdfName=setupPdfs(w,name,sigName,bkgName,nBkgName,nSigName,sigCoefName,plotOn=0)
    # throw toy from pdf
    throwToy(w,bkgPdfName,sigPdfName,bkgName,nInjectSignal=nSig*injectScale,nEvents=nBkg,nToys=nToys)
    # save information about this channel in a dictionary
    channels.append({"name":name,"addPdfName":addPdfName,"sigName":sigName,"origNsigValue":nSig,
                     "bkgName":bkgName,"bkgPdfName":bkgPdfName,"sigPdfName":sigPdfName,
                     "nSigName":nSigName,"nBkgName":nBkgName,"sigCoefName":sigCoefName,
                     "nSigSr":nSigSr, "nBkgSr":nBkgSr,
                     })
    if i>=1 and quick:break;
  print "Done with prefits"
  # quit()
  
  # loop over toys, do combined fits for each toy
  # if you want to fit to data, just set the name of the dataset in channels
  for toyI in range(nToys): # loop over versions of toy
    print "%"*100
    print "%"*100
    print "%%%% Starting evaluation on toy {0}".format(toyI)
    print "%%%% First, do fits for each channel".format(toyI)
    print "%%%% Then make combined dataset".format(toyI)
    print "%%%% Then do NLL fit".format(toyI)
    print "%"*100
    print "%"*100
    # fix variables other than x, mu
    origRanges=fixVariablesInWs(w,skipFix=lambda x: x in ["x","mu"])
    # origRanges=fixVariablesInWs(w,skipFix=lambda x: x in ["x","mu"] or "nEvents" in x)
  
    # set names of datasets to fit
    for chanI,chan in enumerate(channels):
      bkgName=chan["bkgName"]
      # these fits are to data
      # toFitRdhName=bkgName
      # toFitRdsName=bkgName+"Rds"
      # these fits are to toys
      toFitRdhName=bkgName+"ToyRdh"+str(toyI)
      toFitRdsName=bkgName+"ToyRds"+str(toyI)
      channels[chanI]["toFitRdhName"]=toFitRdhName
      channels[chanI]["toFitRdsName"]=toFitRdsName
  
    # do poisson CLs limit (should be done before other fits so nSig is the expected version)
    # requires poissonLimitFunction
    # doLimit(w, channels); continue
  
    # build combined data set from toys
    makeCombinedDataset(w,channels,toyId=toyI,plotOn=0)
  
    # do nll
    doNll(w,toyId=toyI,channels=channels)
  
    # unfix variables for next round of fitting
    unFixVariablesInWs(w,origRanges)
  
  
  # print w.Print()
  print "@"*100
  print "injected\trecovered\n",injectScale,"\t\t",w.var("mu").getVal()
  print "DONE"
  print "(rooFit may exit gracefully with a segfault, but reaching this means the script is done)"
  print "@"*100
  
  os.popen("rm AutoDict*")
